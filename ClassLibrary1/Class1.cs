﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tehtävä2;

namespace ClassLibrary1
{  [TestFixture]

    public class Class1
    {
        MerkkiJonoLaskin laskin;

        [SetUp]
        public void TestienAlustus()
        {
            MerkkiJonoLaskin laskin = new MerkkiJonoLaskin();
        }
   [Test]
   public void TyhjämerkkijonoPalauttaaNollan ()
        {
            MerkkiJonoLaskin laskin = new MerkkiJonoLaskin();

            Assert.That(laskin.Summa(""),Is.EqualTo(0));
        }
        [Test]
        public void YksiAnnettuLukuPalauttaaLuvunArvon()
        {
            MerkkiJonoLaskin laskin = new MerkkiJonoLaskin();
            Assert.That(laskin.Summa("5"), Is.EqualTo(5));
        }

        [Test]
        public void KaksiLukuaPlkullaErotettunaPalauttaaSumman()
        {

            MerkkiJonoLaskin laskin = new MerkkiJonoLaskin();

            Assert.That(laskin.Summa("1,2"), Is.EqualTo(3));
        }

        [Test]
        public void UseampiKuinKaksiLukuaPilkullaErotettunaPalauttaaSumman()
        {
            Assert.That(laskin.Summa("1,2,3,4"), Is.EqualTo(10));

        }
        

    }
}
